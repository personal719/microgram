package Auth.contollers;

import Auth.repositories.UserRepository;
import Auth.security.TokenAuthenticationService;
import Auth.services.ConvertService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@Slf4j
public class AuthController {
    private final AuthenticationManager authenticationManager;
    private final UserDetailsService userDetailsService;
    private final TokenAuthenticationService tokenAuthenticationService;
    private final ConvertService convertService;
    private final UserRepository userRepository;

    public AuthController(@Qualifier("userDetailsServiceApp") UserDetailsService userDetailsService, AuthenticationManager authenticationManager, TokenAuthenticationService tokenAuthenticationService, ConvertService convertService,UserRepository userRepository) {
        this.userDetailsService = userDetailsService;
        this.authenticationManager = authenticationManager;
        this.tokenAuthenticationService = tokenAuthenticationService;
        this.userRepository = userRepository;
        this.convertService = convertService;
    }

    @PostMapping("/login")
    public Object login(@RequestBody Map<String, Object> body) throws Exception {
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(body.get("name"), body.get("password"))
            );
        } catch (BadCredentialsException ex) {
            throw new Exception("incorrect credentials", ex);
        }
        UserDetails name = userDetailsService.loadUserByUsername(body.get("name").toString());
        HashMap<Object, Object> map = new HashMap<>();
        map.put("token", tokenAuthenticationService.generateToken(name.getUsername()));
        return map;
    }

    @GetMapping("/me")
    public Object app() {
        log.info("Try /me");
        Object username = convertService.convertToMap(SecurityContextHolder.getContext().getAuthentication().getPrincipal()).get("username");
        return userRepository.getUserByName(username.toString()).get();
    }

    @GetMapping("/user")
    public Object user() {
        return "User";
    }

    @GetMapping("/admin")
    public Object admin() {
        return "admin";
    }

}
