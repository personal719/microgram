package Auth.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class ConvertService {
    public Map<String,Object> convertToMap(Object obj){
        ObjectMapper oMapper = new ObjectMapper();
        return oMapper.convertValue(obj, Map.class);
    }
}
