package Auth.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.function.Function;

@Component
public class TokenAuthenticationService {

    private long EXPIRATIONTIME = 1000 * 60 * 60 * 24 * 10; // 10 days
    private String secret = "ThisIsASecret";

    public String generateToken(String username) {
        return Jwts.builder()
                .setSubject(username)
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATIONTIME))
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }

    public  <T> T extract(String token, Function<Claims, T> claimsTFunction) {
        Claims body = Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
        return claimsTFunction.apply(body);
    }

    public Boolean validateToken(String token, UserDetailsApp userDetailsApp) {
        String subject = extract(token, Claims::getSubject);
        Date expiration = extract(token, Claims::getExpiration);
        return (subject.equals(userDetailsApp.getUsername()) && ! expiration.before(new Date()));
    }
}
