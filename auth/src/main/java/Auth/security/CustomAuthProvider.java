package Auth.security;

import Auth.modules.User;
import Auth.repositories.UserRepository;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class CustomAuthProvider implements AuthenticationProvider {
    private final UserRepository userRepository;

    public CustomAuthProvider(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Authentication authenticate(Authentication auth) throws AuthenticationException {
        Optional<User> user = userRepository.getUserByNameAndPassword(auth.getName(), (String) auth.getCredentials());
        if (user.isPresent())
            return new UsernamePasswordAuthenticationToken(user.get().getName(), user.get().getPassword());
        else
            throw new BadCredentialsException("External system authentication failed");
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(UsernamePasswordAuthenticationToken.class);
    }
}
