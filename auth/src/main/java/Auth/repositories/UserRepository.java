package Auth.repositories;

import Auth.modules.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User,Integer> {
    Optional<User> getUserByName(String name);
    Optional<User> getUserByNameAndPassword(String name, String password);

}
