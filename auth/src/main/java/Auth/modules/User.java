package Auth.modules;

import lombok.Data;

import javax.persistence.*;

@Entity()
@Table(name = "users")
@Data
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String name;
    private String password;
    private String role;
}
