CREATE TABLE IF NOT EXISTS `users` (
    `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `name` varchar(20),
    `password` varchar(50),
    `role` varchar(10)
)ENGINE=InnoDB DEFAULT CHARSET=UTF8;