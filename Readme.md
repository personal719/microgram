## Descirption
Cloud system project 

## Non functional requirement
* scalability(horizontal && vertical )
* availability
* high performance (achieved using load balancing)

## Results 
![](docs/eureka-server.png)

![](docs/hystrix.png)

![](docs/hystrix-fail.png)

![](docs/success.png)

![](docs/zipkin-front.png)

![](docs/zipkin-pararell.png)

![](docs/zipkin-serial.png)


## Used tools and techniques
* Mysql(8.0)
* Java(1.8)
* [SOLID](https://en.wikipedia.org/wiki/SOLID)
* [Rabbitmq](https://www.rabbitmq.com/)
* spring-boot (2.2.4)