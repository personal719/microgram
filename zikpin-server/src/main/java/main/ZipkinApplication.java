package main;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.sleuth.sampler.AlwaysSampler;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import zipkin.server.EnableZipkinServer;

import java.util.logging.Level;
import java.util.logging.Logger;

@SpringBootApplication
@RestController
@EnableDiscoveryClient
@EnableZipkinServer
public class ZipkinApplication {
    private static final Logger LOG = Logger.getLogger(ZipkinApplication.class.getName());

    public static void main(String[] args) {
        SpringApplication.run(ZipkinApplication.class, args);

    }

    @Bean
    RestTemplate template() {
        return new RestTemplate();

    }

    @Bean
    public AlwaysSampler alwaysSampler() {
        return new AlwaysSampler();
    }

    @GetMapping("/test")
    public Object testLog() {
        LOG.log(Level.INFO, "I hop it's work", "Index API is calling");
        test();
        return "I am test ";
    }

    @GetMapping("/call/test")
    public Object callTest() {
        System.out.println(this.template().getForObject(
                "http://localhost:9090/test",
                String.class
        ));
        LOG.info("I am called test");
        return null;
    }

    private void test() {
        LOG.log(Level.INFO, "Trying-tracing", "Tracing-http");
    }

}