package main.controller;

import lombok.extern.slf4j.Slf4j;
import main.exception.ResourceNotFound;
import main.requests.AcceptReq;
import main.requests.FollowReq;
import main.service.FriendService;
import main.util.CustomResponseMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/managment/follow")
@Slf4j
public class FriendController {
    @Autowired
    private FriendService service;

    @GetMapping("/requests/all")
    public Object getFollowRequests(@RequestHeader("USER_CLAIM_ID") Integer userId) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(new CustomResponseMap()
                        .put("data", new CustomResponseMap()
                                .put("requests", service.getFollowRequests(userId)
                                        .stream().map(f -> new CustomResponseMap()
                                                .put("id", f.getId())
                                                .put("idSender", f.getIdSender())
                                                .put("idReceiver", f.getIdReceiver())))));
    }

    @GetMapping("/followers/all")
    public Object getFollowers(@RequestHeader("USER_CLAIM_ID") Integer userId) {
        log.info("user {} try to find", userId);
        return ResponseEntity.status(HttpStatus.OK)
                .body(new CustomResponseMap()
                        .put("data", new CustomResponseMap()
                                .put("followers", service.getFollowers(userId)
                                        .stream().map(f -> new CustomResponseMap()
                                                .put("id", f.getId())
                                                .put("idSender", f.getIdSender())
                                                .put("idReceiver", f.getIdReceiver())))));
    }

    @PostMapping()
    public Object addFollowRequest(@Validated @RequestBody FollowReq request, BindingResult result, @RequestHeader("USER_CLAIM_ID") Integer userId) {
        if (result.hasErrors())
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(new CustomResponseMap()
                            .put("data", new CustomResponseMap())
                            .put("errors", getErrorsObject(result)));
        service.addFollowRequest(request.senderName, userId, Integer.valueOf(request.receiverId.toString()));
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(new CustomResponseMap()
                        .put("data", new CustomResponseMap())
                        .put("message", "Done"));
    }

    @PutMapping()
    public Object acceptFriend(@Validated @RequestBody AcceptReq request,
                               BindingResult result, @RequestHeader("USER_CLAIM_ID") Integer userId) {
        if (result.hasErrors())
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(new CustomResponseMap()
                            .put("data", new CustomResponseMap())
                            .put("errors", getErrorsObject(result)));
        try {
            service.acceptRequest(Long.valueOf(request.requestId.toString()));
            return ResponseEntity.status(HttpStatus.OK)
                    .body(new CustomResponseMap()
                            .put("data", new CustomResponseMap())
                            .put("message", "Done"));
        } catch (ResourceNotFound e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(new CustomResponseMap()
                            .put("data", new CustomResponseMap())
                            .put("errors", e.getMessage()));
        }

    }

    @DeleteMapping
    @Transactional
    public Object unFollow(@Validated @RequestBody FollowReq request,
                           BindingResult result, @RequestHeader("USER_CLAIM_ID") Integer userId) {
        if (result.hasErrors())
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(new CustomResponseMap()
                            .put("data", new CustomResponseMap())
                            .put("errors", getErrorsObject(result)));
        try {
            service.unFollow(userId, (Integer) request.receiverId);
            return ResponseEntity.status(HttpStatus.OK)
                    .body(new CustomResponseMap()
                            .put("data", new CustomResponseMap())
                            .put("message", "Done"));
        } catch (ResourceNotFound e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(new CustomResponseMap()
                            .put("data", new CustomResponseMap())
                            .put("errors", e.getMessage()));
        }


    }

    public CustomResponseMap getErrorsObject(BindingResult result) {
        CustomResponseMap errors = new CustomResponseMap();
        if (result.getFieldErrors().size() != 0) {
            result.getFieldErrors()
                    .forEach(f -> errors.put(f.getField(), f.getDefaultMessage()));
        } else errors.put(result.getFieldError().getField(), result.getFieldError().getDefaultMessage());
        return errors;
    }

}


