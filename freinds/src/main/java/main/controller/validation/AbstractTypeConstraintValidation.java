package main.controller.validation;

import javax.validation.ConstraintValidatorContext;

public abstract class AbstractTypeConstraintValidation {
    public AbstractTypeConstraintValidation() {
        registerHandleType();
    }

    public abstract boolean handle(Object obj, ConstraintValidatorContext context);

    protected abstract void registerHandleType();


    protected void registerHandleType(String type) {
        FactoryHandleTypeValidation.addHandleValidation(type, this);
    }
}
