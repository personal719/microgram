package main.controller.validation;

import main.controller.validation.AbstractTypeConstraintValidation;

import javax.validation.ConstraintValidatorContext;

public class IntegerValidationAbstract extends AbstractTypeConstraintValidation {

    @Override
    public boolean handle(Object obj, ConstraintValidatorContext context) {
        boolean valid = obj instanceof Integer;
        if (!valid)
            context.buildConstraintViolationWithTemplate(String.format("the value of [%s] is invalid it must be integer", obj)).addConstraintViolation();
        return valid;
    }

    @Override
    public void registerHandleType() {
        super.registerHandleType("int");
    }
}
