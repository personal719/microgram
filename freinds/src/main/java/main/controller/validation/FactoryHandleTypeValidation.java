package main.controller.validation;

import java.util.HashMap;
import java.util.Map;

public class FactoryHandleTypeValidation {
    private final static Map<String, AbstractTypeConstraintValidation> handleTypesMaps = new HashMap<>();

    public static AbstractTypeConstraintValidation getSuitableHandleType(String target) {
        return handleTypesMaps.get(target);
    }

    public static void addHandleValidation(String type, AbstractTypeConstraintValidation typeConstraintValidation) {
        handleTypesMaps.put(type, typeConstraintValidation);
    }
}
