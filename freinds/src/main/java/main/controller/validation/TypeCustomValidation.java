package main.controller.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class TypeCustomValidation implements ConstraintValidator<TypeConstraint, Object> {
    private TypeConstraint constraint;

    public void initialize(TypeConstraint constraint) {
        this.constraint = constraint;
    }

    public boolean isValid(Object obj, ConstraintValidatorContext context) {
        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate(constraint.message());
        return FactoryHandleTypeValidation
                .getSuitableHandleType(constraint.targetType())
                .handle(obj, context);
    }
}

