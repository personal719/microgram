package main.config;

import main.controller.validation.IntegerValidationAbstract;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CustomValidationConfig {
    @Bean
    public IntegerValidationAbstract getIntegerValidation() {
        return new IntegerValidationAbstract();
    }
}

