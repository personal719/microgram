package main.requests;

import main.controller.validation.TypeConstraint;

public class AcceptReq {
    @TypeConstraint(targetType = "int")
    public Object requestId;

}
