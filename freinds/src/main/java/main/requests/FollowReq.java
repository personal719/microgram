package main.requests;


import main.controller.validation.TypeConstraint;

public class FollowReq {
    @TypeConstraint(targetType = "int")
    public Object receiverId;
    public String senderName;

}
