package main.model.repo.messages;

import com.google.gson.Gson;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class NotificationMessage {
    private Integer idSender;
    private Integer idReceiver;
    private String senderName;

    public String toJson() {
        return new Gson()
                .toJson(this);
    }

    public static NotificationMessage fromJson(String str) {
        return new Gson().fromJson(str, NotificationMessage.class);
    }
}
