package main.model.repo;

import main.model.repo.entity.Friend;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface FriendCrud extends CrudRepository<Friend, Long> {

    List<Friend> findAllByIdSenderOrIdReceiverAndIsAcceptedFalse(Integer idSender, Integer idReceiver);

    List<Friend> findAllByIdReceiverAndIsAcceptedTrue(Integer idReceiver);

    Optional<Friend> deleteByIdSenderAndIdReceiver(Integer idSender, Integer idReceiver);

    Optional<Friend> findByIdSenderAndIdReceiver(Integer idSender, Integer idReceiver);

}
