package main.service;


import main.exception.ResourceNotFound;
import main.model.repo.FriendCrud;
import main.model.repo.entity.Friend;
import main.model.repo.messages.NotificationMessage;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class FriendService {
    private final FriendCrud crud;
    private final RabbitServiceSender rabbitServiceSender;
    private final RestTemplate restTemplate;
    private final String URL = "http://auth-service/user/{%s}";

    public FriendService(FriendCrud crud, RabbitServiceSender rabbitServiceSender, RestTemplate restTemplate) {
        this.crud = crud;
        this.rabbitServiceSender = rabbitServiceSender;
        this.restTemplate = restTemplate;
    }

    public void addFollowRequest(String sendername, Integer sender, Integer receiver) {
        Friend friend = new Friend()
                .setIdReceiver(receiver)
                .setIdSender(sender);
        crud.save(friend);
        this.rabbitServiceSender
                .sendNotification(new NotificationMessage()
                        .setIdReceiver(receiver)
                        .setIdSender(sender)
                        .setSenderName(sendername));
    }

    public void acceptRequest(Long requestId) {
        Friend friend = crud.findById(requestId)
                .orElseThrow(() -> new ResourceNotFound(String.format("Request [%s] not found", requestId)));
        friend.setIsAccepted(true);
        crud.save(friend);
        rabbitServiceSender.sendNotification(new NotificationMessage());
    }

    public List<Friend> getFollowRequests(Integer userId) {
        return crud.findAllByIdSenderOrIdReceiverAndIsAcceptedFalse(userId, userId);
    }

    public List<Friend> getFollowers(Integer userId) {
        return crud.findAllByIdReceiverAndIsAcceptedTrue(userId);
    }

    public void unFollow(Integer idSender, Integer idReceiver) {
        crud.findByIdSenderAndIdReceiver(idSender, idReceiver).orElseThrow(() -> new ResourceNotFound("Follower not found in your list"));
        crud.deleteByIdSenderAndIdReceiver(idSender, idReceiver);
    }
}

