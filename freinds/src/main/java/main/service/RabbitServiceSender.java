package main.service;

import main.model.repo.messages.NotificationMessage;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class RabbitServiceSender {
    private final RabbitTemplate rabbitTemplate;
    private final String notificationName;

    public RabbitServiceSender(RabbitTemplate rabbitTemplate, @Value("${rabbit.notification}") String notificationName) {
        this.rabbitTemplate = rabbitTemplate;
        this.notificationName = notificationName;
    }

    public void sendNotification(NotificationMessage notificationMessage) {
        rabbitTemplate.convertAndSend(notificationName, notificationMessage.toJson());
    }
}

