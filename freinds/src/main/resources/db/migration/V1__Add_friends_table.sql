CREATE TABLE friend
(
  id int primary key auto_increment,
  id_sender int not null ,
  id_receiver int not null ,
  is_accepted boolean default false,
  CREATEd_at timestamp  default current_timestamp,
  updated_at timestamp  default current_timestamp on update current_timestamp
);