package main;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

import static java.lang.String.format;

@Service
public class MediaService {
    private final MediaCrud crud;
    private String storagePath;

    public MediaService(MediaCrud mediaRepo, @Value("${storage.path}") String storagePath) {
        this.crud = mediaRepo;
        this.storagePath = storagePath;
    }


    @Transactional
    public Media storeImage(MultipartFile image, String caption, String modelName, Integer modelId, String tableName) throws IOException {
        Media media;
        String fileName = UUID.randomUUID().toString() + ".jpg";
        media = new Media()
                .setName(fileName)
                .setSize(image.getSize())
                .setTableName(tableName)
                .setUserId(modelId)
                .setCaption(caption);
        crud.save(media);
        String path = format("%s/%s/%s/%s", storagePath, modelName, media.getId(), fileName);
        makeDir(path);
        media.setModelId(modelId).setModelName(modelName);
        media.setPath(path);
        media.setUrl(format("/%s/%s", "media/images", media.getId()));
        crud.save(media);
        Path filepath = Paths.get(path, fileName);
        image.transferTo(filepath);
        return media;
    }


    private void makeDir(String dirName) {
        File file = new File(dirName);
        file.mkdirs();
    }

    public Media find(Long id) {
        return crud.findByIdAndIsDeletedIsFalse(id).orElseThrow(() -> new ResourceNotFound(String.format("Media %s not found", id)));
    }

    public void deleteImage(Long id) {
        Media media = crud.findById(id).orElseThrow(() -> new ResourceNotFound(String.format("Media %s not found", id)));

        if (media.isDeleted())
            throw new ResourceNotFound(String.format("Media [%s] deleted before", id));
        media.setDeleted(true);
        crud.save(media);
    }

    public Page<Media> getImages(Integer userId, Pageable pageable) {
        return crud.findAllByModelIdAndIsDeletedIsFalse(userId, pageable);
    }
}

