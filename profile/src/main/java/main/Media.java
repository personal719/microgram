package main;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
@Accessors(chain = true)
public class Media {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Integer userId;
    private String name;
    private String path;
    private double size;
    private String modelName;
    private String tableName;
    private Integer modelId;
    private String url;
    private boolean isDeleted;
    private String caption;
}
