package main;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface MediaCrud extends CrudRepository<Media, Long> {

    Page<Media> findAllByModelIdAndIsDeletedIsFalse(Integer modelId, Pageable pageable);

    Optional<Media> findByIdAndIsDeletedIsFalse(Long id);
}
