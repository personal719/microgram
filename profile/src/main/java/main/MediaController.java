package main;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.*;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

@RestController
@RequestMapping("/media")
@Slf4j
public class MediaController {
    @Autowired
    private MediaService media;
    @Autowired
    private RestTemplate template;

    @GetMapping("/images/{id}")
    public Object getImage(@PathVariable Long id) throws IOException {
        try {
            Media media = this.media.find(id);
            File file = new File(media.getPath() + "/" + media.getName());
            Path path = Paths.get(file.getAbsolutePath());
            ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));
            return ResponseEntity.status(HttpStatus.OK)
                    .contentLength(file.length()).
                            contentType(MediaType.IMAGE_JPEG)
                    .body(resource);
        } catch (ResourceNotFound e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body(new CustomResponseMap()
                            .put("errors", new CustomResponseMap().put("message", e.getMessage()))
                            .put("data", new CustomResponseMap()));
        }
    }

    @GetMapping("/images")
    public Object getImages(@RequestHeader("USER_CLAIM_ID") Integer userId, @PageableDefault Pageable pageable) throws IOException {
        return getImageResponse(this.media.getImages(userId, pageable));
    }

    @DeleteMapping("/images/{id}")
    public Object deleteImage(@PathVariable Long id) throws IOException {
        try {
            this.media.deleteImage(id);
        } catch (ResourceNotFound e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body(new CustomResponseMap()
                            .put("data", new CustomResponseMap())
                            .put("errors", new CustomResponseMap().put("message", e.getMessage())));
        }
        return ResponseEntity.status(HttpStatus.OK)
                .body(new CustomResponseMap().put("data", new CustomResponseMap().put("message", "Done")));
    }

    @Transactional
    @PostMapping()
    public Object create(@RequestHeader("USER_CLAIM_ID") Integer userId, MultipartFile image, @RequestPart(required = false) String caption) throws IOException {
        Media mediaResult = media.storeImage(image, caption, "users", userId, "users");
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(
                        new CustomResponseMap()
                                .put("date", new CustomResponseMap()
                                        .put("media_url", mediaResult.getUrl())
                                        .put("capture", caption))
                );
    }

    @GetMapping("/public")
    public Object publicProfile(@RequestHeader("USER_CLAIM_ID") Integer userId, @PageableDefault Pageable pageable) {
        Stream<Media> allMedia = this.media.getImages(userId, pageable).get();
        HttpHeaders headers = new HttpHeaders();
        headers.set("USER_CLAIM_ID", userId.toString());
        ResponseEntity exchange = template.exchange("http://friend-service/managment/follow/followers/all", HttpMethod.GET, new HttpEntity<>(headers), Object.class);
        return exchange.getBody();
    }


    private CustomResponseMap getImageResponse(Page<Media> mediaPage) {
        return new CustomResponseMap()
                .put("data", new CustomResponseMap()
                        .put("totalPages", mediaPage.getTotalPages())
                        .put("totalElements", mediaPage.getTotalElements())
                        .put("size", mediaPage.getSize())
                        .put("content", mediaPage.get()
                                .map(f -> new CustomResponseMap()
                                        .put("id", f.getId())
                                        .put("url", f.getUrl())
                                        .put("caption", f.getCaption())
                                ))
                );
    }
}

