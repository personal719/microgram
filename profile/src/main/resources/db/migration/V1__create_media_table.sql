CREATE TABLE media(
  id int primary key auto_increment,
  user_id int not null ,
  name varchar (50) not null,
  path varchar (100),
  size double ,
  model_name varchar (100),
  model_id varchar (100),
  table_name varchar (100),
  url varchar (300),
  caption varchar (100),
  is_deleted boolean default false,
  CREATEd_at timestamp  default current_timestamp,
  updated_at timestamp  default current_timestamp
);