package main.filters;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import lombok.extern.slf4j.Slf4j;
import main.moduls.User;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

@Slf4j
public class PreFilter extends ZuulFilter {
    RestTemplate restTemplate;

    public PreFilter(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    private final String URL = "http://auth-service/me";

    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 1;
    }

    @Override
    public boolean shouldFilter() {
        return !(RequestContext.getCurrentContext().getRequest()
                .getRequestURI().endsWith("/login") ||
                RequestContext.getCurrentContext().getRequest()
                        .getRequestURI().endsWith("/ping")) ||
                RequestContext.getCurrentContext().getRequest()
                        .getRequestURI().endsWith("/slueth/**");
    }

    @Override
    public Object run() {

        RequestContext ctx = RequestContext.getCurrentContext();
        if (ctx.getRequest().getHeader("Authorization") == null)
            sendErrorRespond("please send the auth token", 401, ctx);
        User user = getCurrentUser(ctx.getRequest().getHeader("Authorization"));
        if (user == null)
            sendErrorRespond("please send valid auth token", 401, ctx);
        else
            setUserClaim(ctx, user);
        return null;
    }

    private void sendErrorRespond(String message, int code, RequestContext ctx) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("code", code);
            jsonObject.put("message", message);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ctx.setResponseStatusCode(code);
        ctx.setResponseBody(jsonObject.toString());
        ctx.setSendZuulResponse(false);
    }

    private User getCurrentUser(String authToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", authToken);
        HttpEntity request = new HttpEntity(headers);
        try {
            return restTemplate.exchange(
                    URL,
                    HttpMethod.GET,
                    request,
                    User.class,
                    1
            ).getBody();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private void setUserClaim(RequestContext ctx, User user) {
        ctx.addZuulRequestHeader("USER_CLAIM_ID", Integer.toString(user.id));
        ctx.addZuulRequestHeader("USER_CLAIM_USERNAME", user.name);
    }
}
